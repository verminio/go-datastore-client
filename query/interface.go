package query

import (
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/widget"
)

var controlEnterShortcut = desktop.CustomShortcut{KeyName: fyne.KeyReturn, Modifier: desktop.ControlModifier}

type queryEditorEntry struct {
	widget.Entry
}

func (e *queryEditorEntry) onCtrlEnter() {
	log.Println("Run Query!")
}

func (e *queryEditorEntry) TypedShortcut(shortcut fyne.Shortcut) {
	switch shortcut.ShortcutName() {
	case controlEnterShortcut.ShortcutName():
		e.onCtrlEnter()
	default:
		e.Entry.TypedShortcut(shortcut)
	}
}

func NewEditor() *queryEditorEntry {
	editor := &queryEditorEntry{}
	editor.ExtendBaseWidget(editor)
	return editor
}
