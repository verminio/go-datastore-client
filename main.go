package main

import (
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"gitlab.com/verminio/go-datastore-client/action"
	"gitlab.com/verminio/go-datastore-client/query"
)

func main() {
	a := app.New()
	w := a.NewWindow("Hello")

	w.SetContent(container.NewVBox(
		action.NewToolbar(),
		query.NewEditor(),
	))

	w.ShowAndRun()
}
